from django.db import models
from django.urls import reverse

# Create your models here.

class kegiatan(models.Model):
    namakegiatan = models.CharField(max_length=60, default='main')
    hari = models.CharField(max_length=60, default='besok')

    def __str__(self):
        return self.namakegiatan

    def get_absolute_url(self):
        return reverse('story6:index')
    
    
class peserta(models.Model):
    namapeserta = models.CharField(max_length=30)
    pengikut = models.ForeignKey(kegiatan, on_delete=models.CASCADE)

    def __str__(self):
        return self.namapeserta

    def get_absolute_url(self):
        return reverse('story6:index')
    

    