from django.contrib import admin
from .models import kegiatan, peserta


# Register your models here.
admin.site.register(kegiatan)
admin.site.register(peserta)