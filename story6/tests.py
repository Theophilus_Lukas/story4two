from django.test import TestCase, Client
from .models import peserta, kegiatan
from django.urls import reverse

# Create your tests here.
class TestingStory6(TestCase):
    def test_url_show_kegiatan(self):
        response = Client().get('/story6/')
        self.assertEquals(response.status_code, 200)
    
    def test_url_form_peserta(self):
        response = Client().get('/story6/formpeserta/')
        self.assertEquals(response.status_code, 200)

    def test_url_form_kegiatan(self):
        response = Client().get('/story6/formkegiatan/')
        self.assertEquals(response.status_code, 200)

    def test_halaman_showkegiatan_ada_formkegiatan_dan_formpeserta(self):
        response = Client().get('/story6/')
        html_back = response.content.decode('utf8')
        self.assertIn("Tambah Kegiatan", html_back)
        self.assertIn("Tambah Peserta", html_back)

    def test_form_peserta_ada_tombolsubmit(self):
        response = Client().get('/story6/formpeserta/')
        html_back = response.content.decode('utf8')
        self.assertIn("<button>submit</button>", html_back)

    def test_form_kegiatan_ada_tombolsubmit(self):
        response = Client().get('/story6/formkegiatan/')
        html_back = response.content.decode('utf8')
        self.assertIn("<button>submit</button>", html_back)

    def test_model_kegiatanC(self):
        kegiatan.objects.create(namakegiatan="Bermain", hari="besok")
        banyak_kegiatan = kegiatan.objects.all().count()
        self.assertEquals(banyak_kegiatan, 1)
    
    def test_model_pesertaC(self):
        keg = kegiatan.objects.create(namakegiatan="BermainSepeda", hari="besok")
        peserta.objects.create(namapeserta="Toni", pengikut=keg)
        banyak_kegiatan = kegiatan.objects.all().count()
        self.assertEquals(banyak_kegiatan, 1)

    def test_model_kegiatan(self):
        w = kegiatan.objects.create(namakegiatan="Test", hari="test")
        self.assertEquals(w.__str__(), w.namakegiatan)
        self.assertEquals("/story6/", w.get_absolute_url())

    def test_model_peserta(self):
        w = kegiatan.objects.create(namakegiatan="Test2", hari="test2")
        o = peserta.objects.create(namapeserta="tester", pengikut=w)
        self.assertEquals(o.__str__(), o.namapeserta)
        self.assertEquals("/story6/", o.get_absolute_url())