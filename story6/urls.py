from django.urls import path

from .views import EventView, AddEventView, AddPesertaView
from . import views

app_name = 'story6'

urlpatterns = [
    path('', EventView.as_view(), name='index'),
    path('formpeserta/', AddPesertaView.as_view(), name='peserta'),
    path('formkegiatan/', AddEventView.as_view(), name='kegiatan'),

]