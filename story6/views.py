from django.shortcuts import render
from .models import kegiatan, peserta
from django.views.generic import ListView, CreateView

# Create your views here.
class EventView(ListView):
    context_object_name = 'daftar'
    template_name = 'story6/showkegiatan.html'
    queryset = kegiatan.objects.all()

    def get_context_data(self, **kwargs):
        context = super(EventView, self).get_context_data(**kwargs)
        context["peserta"] = peserta.objects.all() 
        return context
    
class AddPesertaView(CreateView):
    model = peserta
    template_name = 'story6/pesertaform.html'
    fields = '__all__'

class AddEventView(CreateView):
    model = kegiatan
    template_name = 'story6/kegiatanform.html'
    fields = '__all__'
