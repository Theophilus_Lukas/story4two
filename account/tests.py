from django.test import TestCase, Client

# Create your tests here.

class Test_story9(TestCase):

    def test_url_register(self):
        response = Client().get('/account/register/')
        self.assertEquals(response.status_code, 200)

    def test_complete_register(self):
            response = Client().get('/account/register/')
            html_kembalian = response.content.decode('utf8')
            self.assertIn("Register", html_kembalian)
            self.assertIn("Username", html_kembalian)
            self.assertIn("Password", html_kembalian)

