from django.test import TestCase, Client
from django.urls import resolve
from .views import index, search
import requests
import json

# Create your tests here.
class TestingStory8(TestCase):
    def test_url_search(self):
        response = Client().get('/story8/')
        self.assertEquals(response.status_code, 200)

    def test_view_search(self):
        found = resolve('/story8/')
        self.assertEqual(found.func, index)

    def test_url_data(self):
        response = Client().get('/story8/data/?q=test')
        self.assertEquals(response.status_code, 200)

    def test_template_lengkap(self):
        response = Client().get('/story8/')
        html_back = response.content.decode('utf8')
        self.assertIn("searchbar", html_back)
        self.assertIn("show", html_back)
        self.assertIn("javascript", html_back)

    def test_json_data_getter_exist(self):
        response = Client().get('/story8/data/?q=')
        self.assertEquals(response.status_code, 200)

        self.assertJSONEqual(
            str(response.content, encoding='utf8'),
            {"error": 
            {"code": 400, "message": "Missing query.", "errors": 
            [
                {"message": "Missing query.", 
                "domain": "global", 
                "reason": "queryRequired", 
                "location": "q", 
                "locationType": "parameter"}
            ]
            }
            }
        )

