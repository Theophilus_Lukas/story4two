from django.shortcuts import render
from django.http import JsonResponse
import requests
import json

# Create your views here.
def index(request):
    return render(request, 'story8/searchapi.html')

def search(request):
    searchword = request.GET['q']
    url_dest = 'https://www.googleapis.com/books/v1/volumes?q=' + searchword

    result = requests.get(url_dest)
    data = json.loads(result.content)
    return JsonResponse(data, safe=False)