from django.urls import path

from .views import showpelajaranView, pelajarandetailsView, deletepelajaranView
from . import views

app_name = 'home'

urlpatterns = [
    path('', views.index, name='index'),
    path('index/', views.index, name='index'),
    path('photo/', views.photo, name='photo'),
    path('story1/', views.story1, name='story1'),
    path('formpelajaran/', views.formpelajaran, name='formpelajaran'),
    path('showpelajaran/', showpelajaranView.as_view(), name='showpelajaran'),
    path('pelajarandetails/<int:pk>', pelajarandetailsView.as_view(), name='pelajarandetails'),
    path('pelajarandetails/<int:pk>/deletepelajaran', deletepelajaranView.as_view(), name='deletepelajaran'),
]
