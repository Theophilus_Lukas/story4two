from django.db import models

# Create your models here.
class pelajaran(models.Model):
    nama = models.CharField(max_length=30, default='nama pelajaran')
    dosen = models.CharField(max_length=75, default='me')
    sks = models.CharField(max_length=10, default='24')
    tahun = models.CharField(max_length=20, default='semester / tahun')
    deskripsi = models.TextField()
    ruang = models.CharField(max_length=20, default='where it be?')