from django.test import TestCase, Client
from .models import pelajaran

# Create your tests here.
class TestingStory1(TestCase):
    def test_url_app1(self):
        response = Client().get('/')
        self.assertEquals(response.status_code, 200)

    def test_url_app2(self):
        response = Client().get('/photo/')
        self.assertEquals(response.status_code, 200)
    
    def test_url_app3(self):
        response = Client().get('/story1/')
        self.assertEquals(response.status_code, 200)

    def test_url_app4(self):
        response = Client().get('/formpelajaran/')
        self.assertEquals(response.status_code, 200)

    def test_url_app5(self):
        response = Client().get('/showpelajaran/')
        self.assertEquals(response.status_code, 200)

    def test_model_pelajaranC(self):
        pelajaran.objects.create(nama="ppw", dosen="test", sks="1", tahun="1", deskripsi="test", ruang="test")
        banyak_pelajaran = pelajaran.objects.all().count()
        self.assertEquals(banyak_pelajaran, 1)
    
    
