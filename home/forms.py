from django import forms
from .models import pelajaran

class Pelajaranform(forms.ModelForm):
    class Meta:
        model = pelajaran
        fields = [
            'nama',
            'dosen',
            'sks',
            'tahun',
            'deskripsi',
            'ruang'
        ]
        widgets = {
            'nama': forms.TextInput(attrs={'class': 'form-control'}),
            'dosen': forms.TextInput(attrs={'class': 'form-control'}),
            'sks': forms.TextInput(attrs={'class': 'form-control'}),
            'tahun': forms.TextInput(attrs={'class': 'form-control'}),
            'deskripsi' : forms.Textarea(attrs={'class': 'form-control'}),
            'ruang': forms.TextInput(attrs={'class': 'form-control'})
        }

