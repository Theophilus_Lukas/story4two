from django.shortcuts import render
from django.views.generic import ListView, DetailView, DeleteView
from django.urls import reverse_lazy

from .models import pelajaran
from .forms import Pelajaranform

# Create your views here.

def index(request):
    return render(request, 'home/index.html')

def photo(request):
    return render(request, 'home/photo.html')

def story1(request):
    return render(request, 'home/story1.html')

def formpelajaran(request):
    my_form = Pelajaranform(request.POST or None)
    if my_form.is_valid():
        my_form.save()

    context = {
        'form': my_form
    }
    return render(request, 'home/formpelajaran.html', context)

class showpelajaranView(ListView):
    model = pelajaran
    template_name = 'home/showpelajaran.html'

class pelajarandetailsView(DetailView):
    model = pelajaran
    template_name = 'home/pelajarandetails.html'

class deletepelajaranView(DeleteView):
    model = pelajaran
    template_name = 'home/deletepelajaran.html'
    success_url = reverse_lazy('home:showpelajaran')