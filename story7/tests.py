from django.test import TestCase, Client
from django.urls import resolve
from .views import index

# Create your tests here.
class TestingStory7(TestCase):
    def test_url_accordion(self):
        response = Client().get('/story7/')
        self.assertEquals(response.status_code, 200)

    def test_view_accordion(self):
        found = resolve('/story7/')
        self.assertEqual(found.func, index)
